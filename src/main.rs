use anyhow::{bail, Context, Error};
use async_cell::sync::AsyncCell;
use futures::executor::block_on;
use futures::prelude::*;
use iced::{
    button, image as image_ui, slider, Application, Clipboard, Column, Command, Element, Radio,
    Settings, Slider, Text,
};
use image::hdr::{HdrDecoder, HdrEncoder};
use image::{ImageBuffer, Rgb};
use nalgebra as na;
use render::Render;
use sidequest_core::{self as sqc, sky, ViewParams};
use std::{fmt::Display, fs::File, path::PathBuf, sync::Arc, thread};

pub mod render;

type Engine = sqc::Engine<Vec<f32>>;
type Sky = sky::Sky<Vec<f32>>;
type SkyDistr = sky::SkyDistr<Vec<f32>>;

#[derive(Clone, Debug)]
pub struct Preview {
    pub width: u32,
    pub height: u32,
    pub bytes: Vec<u8>,
}

pub fn main() {
    let _ = SidequestGui::run(Settings {
        window: Default::default(),
        flags: structopt::StructOpt::from_args(),
        ..Default::default()
    });
}

fn parse_samples(s: &str) -> Result<sqc::path::StrategySamples, Error> {
    let mut parts = s.split(',');
    let samples = sqc::path::StrategySamples {
        light_to_surface: parts
            .next()
            .context("missing `light to surface`")?
            .parse()?,
        diffuse_to_light: parts
            .next()
            .context("missing `diffuse to light`")?
            .parse()?,
        gloss_to_light: parts.next().context("missing `gloss to light`")?.parse()?,
    };
    if let Some(extra) = parts.next() {
        bail!("unknown strategy for count: {:?}", extra);
    }
    Ok(samples)
}

fn and_some<T>(b: bool, v: T) -> Option<T> {
    match b {
        true => Some(v),
        false => None,
    }
}

#[derive(Debug, Default, structopt::StructOpt)]
#[structopt(name = "sidequest", about = "Sam's WIP renderer")]
struct Options {
    /// Output file for renders
    #[structopt(short = "o", parse(from_os_str))]
    output: Option<PathBuf>,

    /// Environment map
    #[structopt(short = "e", parse(from_os_str))]
    environment: Option<PathBuf>,

    /// Number of samples per strategy per pixel
    #[structopt(short = "n", default_value = "5,2,1", parse(try_from_str = parse_samples))]
    samples: sqc::path::StrategySamples,

    /// Include diffuse objects in scene
    #[structopt(short = "i")]
    include: bool,
}

#[derive(Clone, Debug)]
pub enum Message {
    Noop,
    SetEngine(Engine),
    SetExposure(f32),
    SetShowRender(bool),
    SetDiffVar(sqc::DiffVariable),
    SetPan(f32),
    SetTilt(f32),
    StartRender,
    RenderUpdate(Render),
    PreviewUpdate(Preview),
    SaveRender,
}

type PreviewArgs = (Render, f32, bool);

struct SidequestGui {
    output: Option<PathBuf>,
    engine: Arc<Engine>,
    exposure: f32,
    show_render: bool,
    last_var: sqc::DiffVariable,
    elevation: f32,
    azimuth: f32,
    render_version: usize,
    render: Option<Render>,
    next_render: Arc<AsyncCell<Render>>,
    next_render_args: Arc<AsyncCell<Render>>,
    preview: Option<Preview>,
    next_preview: Arc<AsyncCell<Preview>>,
    next_preview_args: Arc<AsyncCell<PreviewArgs>>,
    exposure_state: slider::State,
    pan_state: slider::State,
    tilt_state: slider::State,
    save_btn_state: button::State,
}

impl Application for SidequestGui {
    type Executor = iced::executor::Default;
    type Message = Message;
    type Flags = Options;

    fn new(opts: Options) -> (SidequestGui, Command<Self::Message>) {
        let gui = SidequestGui {
            output: opts.output,
            engine: Arc::new(Engine {
                sky: SkyDistr::new(
                    Sky {
                        #[rustfmt::skip]
                    radiance: vec![
                        1., 1., 1.,
                        1., 1., 1.,
                        1., 1., 1.,
                        1., 1., 1.,
                        0., 1., 1.,
                        1., 0., 1.,
                        1., 1., 0.,
                        1., 1., 1.,
                        1., 0., 0.,
                        0., 1., 0.,
                        0., 0., 1.,
                        0., 0., 0.,
                    ],
                        width: 4,
                        height: 3,
                        filtering: sky::Filtering::Bilinear,
                        units: 1.,
                    },
                    |len| vec![0.; len],
                    4,
                    3,
                ),
                samples: opts.samples,
                include: opts.include,
            }),
            exposure: 0.,
            show_render: true,
            last_var: sqc::DiffVariable::Radius,
            elevation: 30.,
            azimuth: 0.,
            render_version: 0,
            render: None,
            next_render: AsyncCell::shared(),
            next_render_args: AsyncCell::shared(),
            preview: None,
            next_preview: AsyncCell::shared(),
            next_preview_args: AsyncCell::shared(),
            exposure_state: slider::State::new(),
            pan_state: slider::State::new(),
            tilt_state: slider::State::new(),
            save_btn_state: button::State::new(),
        };

        let preview = gui.next_preview.clone();
        let preview_args = gui.next_preview_args.take_weak();
        thread::spawn(move || loop {
            use sqc::aces::*;

            let (r, exp, sr) = match block_on(&preview_args) {
                Some(args) => args,
                None => return,
            };

            let width = r.view.width;
            let height = r.view.height;
            let mut bytes = vec![0u8; width as usize * height as usize * 4];
            for (out, est) in bytes.chunks_mut(4).zip(r.buf) {
                let est = sqc::estimate::Estimator::finish(&est);
                if sr
                /* show render */
                {
                    write_bgra(est.intercept, out, exp);
                } else {
                    write_ldr_bgra((est.slope * exp).add_scalar(0.5), out);
                }
            }

            preview.set(Preview {
                width,
                height,
                bytes,
            });
        });

        render::render_worker(gui.next_render_args.take_weak(), gui.next_render.clone());

        let mut init = vec![
            gui.next_render
                .take_shared()
                .map(|r| Message::RenderUpdate(r))
                .into(),
            gui.next_preview
                .take_shared()
                .map(|r| Message::PreviewUpdate(r))
                .into(),
            futures::FutureExt::map(
                smol::Timer::after(std::time::Duration::from_millis(100)),
                |_| Message::StartRender,
            )
            .into(),
        ];

        let samples = opts.samples;
        let include = opts.include;
        if let Some(env_path) = opts.environment {
            init.push(
                blocking_err(format!("loading {}", env_path.display()), move || {
                    use std::io::BufReader;

                    let decode = HdrDecoder::new(BufReader::new(File::open(env_path)?))?;
                    let meta = decode.metadata();
                    let mut energy = Vec::new();
                    energy.reserve(meta.width as usize * meta.height as usize * 3);
                    for p in decode {
                        energy.extend_from_slice(&p?.to_hdr().0);
                    }

                    let sky = Sky {
                        radiance: energy,
                        filtering: if meta.width > 1024 {
                            sky::Filtering::Closest
                        } else {
                            sky::Filtering::Bilinear
                        },
                        width: meta.width,
                        height: meta.height,
                        units: meta.exposure.unwrap_or(1.),
                    };

                    Ok(Message::SetEngine(Engine {
                        sky: SkyDistr::new(sky, |len| vec![0.; len], 256, 128),
                        samples,
                        include,
                    }))
                })
                .into(),
            );
        }

        (gui, Command::batch(init))
    }

    fn title(&self) -> String {
        String::from("sidequest")
    }

    fn update(&mut self, msg: Message, _: &mut Clipboard) -> Command<Message> {
        use Message::*;

        match msg {
            SetEngine(e) => {
                self.engine = Arc::new(e);
                self.render();
            }
            SetExposure(x) => {
                self.exposure = x;
                self.preview();
            }
            SetShowRender(x) => {
                self.show_render = x;
                self.preview();
            }
            SetDiffVar(x) => {
                self.show_render = false;
                if x != self.last_var {
                    self.last_var = x;
                    self.render();
                } else {
                    self.preview();
                }
            }
            SetPan(x) => {
                self.azimuth = x;
                self.render();
            }
            SetTilt(x) => {
                self.elevation = x;
                self.render();
            }
            StartRender => self.render(),
            RenderUpdate(r) => {
                if r.version == self.render_version {
                    self.next_render_args.or_set(r.clone());
                }

                self.render = Some(r);
                self.preview();

                return self
                    .next_render
                    .take_shared()
                    .map(|r| RenderUpdate(r))
                    .into();
            }
            PreviewUpdate(p) => {
                self.preview = Some(p);
                return self
                    .next_preview
                    .take_shared()
                    .map(|p| PreviewUpdate(p))
                    .into();
            }
            SaveRender => match (&self.output, &self.render, &self.preview) {
                (Some(o), Some(r), _) if o.extension() == Some("hdr".as_ref()) => {
                    let o = o.clone();
                    let r = r.clone();
                    return blocking_err("saving HDR image", move || {
                        let enc = HdrEncoder::new(File::create(o)?);
                        let rgb: Vec<_> = r
                            .buf
                            .iter()
                            .map(|est| Rgb(sqc::estimate::Estimator::finish(est).intercept.into()))
                            .collect();
                        enc.encode(&rgb, r.view.width as usize, r.view.height as usize)?;
                        Ok(Message::Noop)
                    })
                    .into();
                }
                (Some(o), _, Some(p)) => {
                    let o = o.clone();
                    let w = p.width;
                    let h = p.height;
                    let b = p.bytes.clone();
                    return blocking_err("saving LDR image", move || {
                        image::DynamicImage::ImageBgra8(
                            ImageBuffer::from_raw(w, h, b).expect("invalid buffer"),
                        )
                        .into_rgb8()
                        .save(o)?;
                        Ok(Message::Noop)
                    })
                    .into();
                }
                _ => (),
            },
            _ => (),
        }

        Command::none()
    }

    fn view(&mut self) -> Element<Message> {
        let mut col = Column::new();
        if let Some(p) = &self.preview {
            col = col.push(image_ui::Image::new(image_ui::Handle::from_pixels(
                p.width,
                p.height,
                p.bytes.clone(),
            )));
        }

        col = col
            .push(Slider::new(
                &mut self.tilt_state,
                -90.0..=90.0,
                self.elevation,
                Message::SetTilt,
            ))
            .push(Slider::new(
                &mut self.pan_state,
                0.0..=360.0,
                self.azimuth,
                Message::SetPan,
            ))
            .push(Slider::new(
                &mut self.exposure_state,
                -6.0..=6.0,
                self.exposure,
                Message::SetExposure,
            ))
            .push(Radio::new(
                true,
                "Show render",
                Some(self.show_render),
                Message::SetShowRender,
            ))
            .push(Radio::new(
                sqc::DiffVariable::Radius,
                "Show derivative to radius",
                and_some(!self.show_render, self.last_var),
                Message::SetDiffVar,
            ))
            .push(Radio::new(
                sqc::DiffVariable::Roughness,
                "Show derivative to roughness",
                and_some(!self.show_render, self.last_var),
                Message::SetDiffVar,
            ));

        if self.output.is_some() {
            col = col.push(
                button::Button::new(&mut self.save_btn_state, Text::new("Save"))
                    .on_press(Message::SaveRender),
            );
        }

        col.into()
    }
}

impl SidequestGui {
    fn render(&mut self) {
        let (x, y) = self.azimuth.to_radians().sin_cos();
        let (z, r) = self.elevation.to_radians().sin_cos();
        let engine = self.engine.clone();
        self.render_version = self.render_version.wrapping_add(1);
        let version = self.render_version;
        let view = ViewParams {
            perspective: sqc::Perspective {
                left: -2.,
                right: 2.,
                top: 1.,
                bottom: -1.,
                near: 2.,
                far: 100.,
            },
            isometry: na::Isometry3::face_towards(
                &(10. * na::Point3::new(x * r, y * r, z)),
                &na::Point3::new(0., 0., 0.),
                &na::Vector3::new(0., 0., 1.),
            )
            .into(),
            diff: self.last_var,
            width: 1024,
            height: 512,
        };

        self.next_render_args.set(Render {
            version,
            buf: vec![sqc::Estimator::new(); view.width as usize * view.height as usize],
            engine,
            view,
        });
    }

    fn preview(&mut self) {
        if let Some(r) = self.render.clone() {
            self.next_preview_args
                .set((r, self.exposure.exp2(), self.show_render));
        }
    }
}

fn blocking_err(
    ctx: impl Display + Send + 'static,
    f: impl FnOnce() -> Result<Message, Error> + Send + 'static,
) -> impl Future<Output = Message> {
    let cell = AsyncCell::shared();
    let future = cell.take_shared();
    thread::spawn(move || match f() {
        Ok(m) => cell.set(m),
        Err(e) => {
            println!("Error while {}: {:?}", ctx, e);
            cell.set(Message::Noop)
        }
    });
    future
}
