use anyhow::Error;
use async_cell::sync::{AsyncCell, TakeWeak};
use futures::executor::block_on;
use sidequest_core::{self as sqc, Engine, ViewParams};
use sqc::Estimator as Est;
use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct Render {
    pub version: usize,
    pub engine: Arc<Engine<Vec<f32>>>,
    pub view: ViewParams,
    pub buf: Vec<Est>,
}

type Input = TakeWeak<Render>;
type Output = Arc<AsyncCell<Render>>;

pub fn cpu_render(input: Input, output: Output) {
    use rayon::prelude::*;

    loop {
        let mut r = match block_on(&input) {
            Some(input) => input,
            None => return,
        };

        let seed = rand::random();
        let v = &r.view;
        let e = &r.engine;
        let w = v.width as usize;
        r.buf.par_iter_mut().enumerate().for_each(|(i, o)| {
            let x = i % w;
            let y = i / w;
            let mut rng = sqc::util::rng_at(seed, x, y);
            e.estimate_ray(v.camera_ray(x, y), &mut rng, v.diff, o);
        });

        output.set(r);
    }
}

#[cfg(feature = "cuda")]
pub fn cuda_render(input: Input, output: Output) -> Result<(), Error> {
    use rustacuda::launch;
    use rustacuda::memory::DeviceBox;
    use rustacuda::prelude::*;
    use sqc::{util::DeviceBufferRef, KernelParams};
    use std::cell::UnsafeCell;
    use std::ffi::CString;

    fn buffer_ref<'a, T>(buf: &'a mut DeviceBuffer<T>) -> DeviceBufferRef<'a, T> {
        DeviceBufferRef::from_raw(buf.as_device_ptr(), buf.len() as u32)
    }

    fn transmute_cell<T>(a: &mut [T]) -> &mut [UnsafeCell<T>] {
        // Sound because:
        // - UnsafeCell<T> is ABI-transparent over T
        // - The reference lifetime and mutability match
        // - Derefs of the cell must be separately proven with `unsafe`
        unsafe { std::mem::transmute(a) }
    }

    // Create a context associated to this device
    // TODO: keep alive
    rustacuda::init(CudaFlags::empty())?;
    let device = Device::get_device(0)?;
    let _context =
        Context::create_and_push(ContextFlags::MAP_HOST | ContextFlags::SCHED_AUTO, device)?;

    // Load PTX module
    let module_data = CString::new(include_str!(env!("KERNEL_PTX_PATH")))?;
    let kernel = Module::load_from_string(&module_data)?;

    // Create a stream to submit work to
    let stream = Stream::new(StreamFlags::NON_BLOCKING, None)?;

    loop {
        let mut r = match input.upgrade() {
            Some(input) => block_on(input.take()),
            None => return,
        };

        // Convert engine
        let mut gpu_engine = r.engine.map_buffer(&mut |s| {
            assert!(!s.is_empty());
            DeviceBuffer::from_slice(s).expect("could not transfer engine buffers")
        }); // TODO: try

        // Create output
        let out_cells = transmute_cell(&mut r.buf);
        let mut gpu_out = DeviceBuffer::from_slice(out_cells)?;

        // Crate parameters
        let mut params = DeviceBox::new(&KernelParams {
            seed: rand::random(),
            view: r.view.clone(),
            engine: gpu_engine.map_buffer_mut(&mut |s| buffer_ref(s)),
            buffer: buffer_ref(&mut gpu_out),
        })?;

        // Do rendering
        let threads = (16, 8);
        let ceil_div = |x, y| (x + y - 1) / y;
        let blocks = (
            ceil_div(r.view.width, threads.0),
            ceil_div(r.view.height, threads.1),
        );

        unsafe {
            launch!(kernel.render<<<blocks, threads, 0, stream>>>(params.as_device_ptr()))?;
        }

        // The kernel launch is asynchronous, so we wait for the kernel to finish executing
        stream.synchronize()?;

        // Copy the output back to the host
        drop(params);
        gpu_out.copy_to(out_cells)?;

        // Return output
        drop(out_cells);
        output.set(r);
    }
}

#[cfg(not(feature = "cuda"))]
pub fn cuda_render(_input: Input, _output: Output) -> Result<(), Error> {
    Err(anyhow::anyhow!("not compiled with the \"cuda\" feature"))
}

pub fn render_worker(input: Input, output: Output) {
    std::thread::spawn(|| {
        if let Err(e) = cuda_render(input.clone(), output.clone()) {
            println!("Could not use GPU rendering: {:?}", e);
            cpu_render(input, output);
        }
    });
}
