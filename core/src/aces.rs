// This fit for the ACES curve was created by Stephen Hill (@self_shadow).

use crate::util::Vec3;
use nalgebra::Matrix3;

#[rustfmt::skip]
pub fn input_mat() -> Matrix3<f32> {
    Matrix3::new(
        0.59719, 0.35458, 0.04823,
        0.07600, 0.90834, 0.01566,
        0.02840, 0.13383, 0.83777,
    )
}

#[rustfmt::skip]
pub fn output_mat() -> Matrix3<f32> {
    Matrix3::new(
        1.60475, -0.53108, -0.07367,
        -0.10208, 1.10813, -0.00605,
        -0.00327, -0.07276, 1.07602,
    )
}

pub fn rtt2odt(v: f32) -> f32 {
    let a = v * (v + 0.0245786) - 0.000090537;
    let b = v * (0.983729 * v + 0.4329510) + 0.238081;
    a / b
}

pub fn to_ldr(value: Vec3) -> Vec3 {
    output_mat() * (input_mat() * value).map(rtt2odt)
}

pub fn write_bgra(high: Vec3, bgra: &mut [u8], exposure: f32) {
    write_ldr_bgra(to_ldr(high * exposure), bgra);
}

pub fn write_ldr_bgra(low: Vec3, bgra: &mut [u8]) {
    let low = low.map(|x| (x.min(1.).max(0.) * 255.) as u8);
    bgra.copy_from_slice(&[low[2], low[1], low[0], 255]);
}

#[test]
#[rustfmt::skip]
fn nalgebra_new_row_major() {
    use crate::util::vec3;

    let m = Matrix3::new(
        0., 0., 1.,
        0., 0., 0.,
        0., 0., 0.,
    );
    assert_eq!(m * vec3(0., 0., 1.,), vec3(1., 0., 0.));
}
