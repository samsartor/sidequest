use crate::{util, KernelParams};

#[no_mangle]
pub extern "ptx-kernel" fn render(params_ptr: *mut KernelParams) {
    use core::arch::nvptx::*;

    let params = unsafe { &*params_ptr };
    let x = unsafe { _block_dim_x() * _block_idx_x() + _thread_idx_x() } as usize;
    let y = unsafe { _block_dim_y() * _block_idx_y() + _thread_idx_y() } as usize;
    let w = params.view.width as usize;
    let h = params.view.height as usize;
    if x >= w || y >= h {
        return;
    }

    let engine = params.engine.map_buffer(&mut |b| unsafe { b.as_slice() });
    let mut rng = util::rng_at(params.seed as usize, x, y);
    let out = unsafe { params.buffer.get_mut(x + y * w) };
    engine.estimate_ray(
        params.view.camera_ray(x, y),
        &mut rng,
        params.view.diff,
        out,
    );
}

#[panic_handler]
fn panic(_info: &::core::panic::PanicInfo) -> ! {
    use core::arch::nvptx::*;

    unsafe { trap() }
}
