use crate::util::{Cpy, Vec3};
use crate::Spectrum;
use nalgebra as na;
use rustacuda_derive::DeviceCopy;

pub trait Estimator<P>: Send {
    type Estimate;

    fn start() -> Self
    where
        Self: Sized;

    fn accumulate(&mut self, at: P, p_at: f32, sample: Spectrum, multiplier: f32);

    fn finish(&self) -> Self::Estimate;
}

#[derive(Copy, Clone, DeviceCopy, Debug)]
#[repr(C)]
pub struct ConstantEstimator {
    pub omega: f32,
    pub mean: Cpy<Spectrum>,
}

impl ConstantEstimator {
    pub fn new() -> Self {
        ConstantEstimator {
            omega: 0.,
            mean: Vec3::zeros().into(),
        }
    }

    fn accumulate_internal(&mut self, sample: Spectrum, weight: f32) -> f32 {
        self.omega += weight;
        let over = self.omega.recip();
        let mut mean = self.mean.to();
        // Welford's method for computing weight mean
        mean += weight * over * (sample - mean);
        self.mean = mean.into();

        over
    }
}

impl<P> Estimator<P> for ConstantEstimator {
    type Estimate = Spectrum;

    fn start() -> Self {
        ConstantEstimator::new()
    }

    fn accumulate(&mut self, _: P, p_at: f32, sample: Spectrum, multiplier: f32) {
        self.accumulate_internal(sample * multiplier, p_at.recip());
    }

    fn finish(&self) -> Spectrum {
        self.mean.to()
    }
}

#[derive(Copy, Clone, DeviceCopy, Debug)]
#[repr(C)]
pub struct LeastSquaresEstimator {
    pub triangle: [f32; 5],
    pub moments: [Cpy<Spectrum>; 3],
}

impl LeastSquaresEstimator {
    pub fn new() -> Self {
        LeastSquaresEstimator {
            triangle: [0.; 5],
            moments: [Spectrum::zeros().into(); 3],
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct LinearEstimate {
    pub intercept: Spectrum,
    pub slope: Spectrum,
}

impl Estimator<f32> for LeastSquaresEstimator {
    type Estimate = LinearEstimate;

    fn start() -> Self {
        LeastSquaresEstimator::new()
    }

    fn accumulate(&mut self, at: f32, p_at: f32, sample: Spectrum, multiplier: f32) {
        let w = p_at.recip();
        let mut t = w;
        for i in 0..self.triangle.len()
        /* x^0 ..= x^4 */
        {
            self.triangle[i] += t;
            if i < self.moments.len() {
                self.moments[i] += t * multiplier * sample;
            }

            t *= at;
        }
    }

    fn finish(&self) -> LinearEstimate {
        let [a, b, c, d, e] = self.triangle;
        let mut intercept = Spectrum::zeros();
        let mut slope = Spectrum::zeros();

        let h = na::Matrix3::new(a, b, c, b, c, d, c, d, e);

        match h.try_inverse() {
            Some(h) => {
                for i in 0..3 {
                    let beta =
                        h * na::Vector3::from_iterator(self.moments.iter().map(|m| m.inner[i]));
                    intercept[i] = beta[0];
                    slope[i] = beta[1];
                }
            }
            None => {
                let scale = self.triangle[0].recip();
                intercept = self.moments[0].to() * scale;
            }
        }

        LinearEstimate { intercept, slope }
    }
}
