use rustacuda_derive::DeviceCopy;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum Strategy {
    LightToSurface,
    DiffuseToLight,
    GlossToLight,
}

use crate::Spectrum;
use Strategy::*;

impl Strategy {
    pub const ALL: &'static [Strategy] = &[LightToSurface, DiffuseToLight, GlossToLight];
}

#[derive(Copy, Clone, Default, Debug, PartialEq, Eq, DeviceCopy)]
#[repr(C)]
pub struct StrategySamples {
    pub light_to_surface: u32,
    pub diffuse_to_light: u32,
    pub gloss_to_light: u32,
}

impl StrategySamples {
    pub fn total(self) -> u32 {
        self.light_to_surface + self.diffuse_to_light + self.gloss_to_light
    }

    pub fn iter(self) -> impl Iterator<Item = Strategy> {
        use core::iter::repeat;

        // TODO: more efficient iterator?
        repeat(LightToSurface)
            .take(self.light_to_surface as usize)
            .chain(repeat(DiffuseToLight).take(self.diffuse_to_light as usize))
            .chain(repeat(GlossToLight).take(self.gloss_to_light as usize))
    }
}

impl core::ops::Index<Strategy> for StrategySamples {
    type Output = u32;

    fn index(&self, strat: Strategy) -> &u32 {
        match strat {
            LightToSurface => &self.light_to_surface,
            DiffuseToLight => &self.diffuse_to_light,
            GlossToLight => &self.gloss_to_light,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Path {
    pub actual: Strategy,
    pub p_light_to_surface: f32,
    pub p_diffuse_to_light: f32,
    pub p_gloss_to_light: f32,
    pub energy: Spectrum,
}

impl core::ops::Index<Strategy> for Path {
    type Output = f32;

    fn index(&self, strat: Strategy) -> &f32 {
        match strat {
            LightToSurface => &self.p_light_to_surface,
            DiffuseToLight => &self.p_diffuse_to_light,
            GlossToLight => &self.p_gloss_to_light,
        }
    }
}

impl Path {
    /// = Weight (multiple importance sampling) / sample probability (monte carlo)
    pub fn multiplier(&self, samples: StrategySamples) -> f32 {
        fn power_beta(x: f32) -> f32 {
            x * x
        }

        fn power_beta_minus_one(x: f32) -> f32 {
            x
        }

        // Based on:
        // - http://www.pbr-book.org/3ed-2018/Light_Transport_III_Bidirectional_Methods/Bidirectional_Path_Tracing.html#MultipleImportanceSampling
        // - http://www.pbr-book.org/3ed-2018/Monte_Carlo_Integration/Importance_Sampling.html
        // - https://graphics.stanford.edu/courses/cs348b-03/papers/veach-chapter9.pdf

        // Notice the path is only either s=0, t=2 or s=1, t=1. That simplifies
        // things greatly. In fact, we'll use the power heuristic rather than the
        // balance heuristic and add a second camera -> light strategy.

        let mut divisor = 0.;

        // Hypothetical strategies
        for &s in Strategy::ALL {
            divisor += power_beta(samples[s] as f32 * self[s]);
        }

        // Divide actual strategy
        power_beta_minus_one(self[self.actual]) * power_beta(samples[self.actual] as f32) / divisor
    }
}
