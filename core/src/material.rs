use crate::util::{vec2, vec3, FloatExt, Vec2, Vec3, Vec3Ext};
use core::f32::consts::PI;
use nalgebra as na;
#[allow(unused_imports)]
use num_traits::Float;

pub trait MicrofacetModel {
    fn lambda(&self, dir: Vec3) -> f32;
    fn d(&self, half: Vec3) -> f32;
    fn sample(&self, out: Vec3, u: Vec2) -> Vec3;

    fn g1(&self, out: Vec3) -> f32 {
        (1. + self.lambda(out)).recip()
    }

    fn g(&self, out: Vec3, input: Vec3) -> f32 {
        (1. + self.lambda(out) + self.lambda(input)).recip()
    }

    fn pdf(&self, out: Vec3, half: Vec3) -> f32 {
        self.d(half) * self.g1(out) * out.dot(&half).max(0.) / out.abs_cos_theta().max(1e-9)
    }
}

pub struct Ggx {
    pub alpha: Vec2,
}

// TODO: this is broken or something
impl MicrofacetModel for Ggx {
    fn lambda(&self, direction: Vec3) -> f32 {
        let a2 = self.alpha.component_mul(&self.alpha);
        let v2 = direction.component_mul(&direction);
        ((1. + a2.dot(&v2.xy()) / v2.z).sqrt() - 1.) / 2.
    }

    fn d(&self, normal: Vec3) -> f32 {
        let n2 = normal.component_mul(&normal);
        let a2 = self.alpha.component_mul(&self.alpha);

        let d_inner = n2.x / a2.x + n2.y / a2.y + n2.z;
        (PI * self.alpha.x * self.alpha.y * d_inner * d_inner).recip()
    }

    fn sample(&self, out: Vec3, u: Vec2) -> Vec3 {
        // Sample the VNDF for the GGX microfacet model
        // See http://jcgt.org/published/0007/04/01/paper.pdf page 10

        // Section 3.2: transforming the view direction to the hemisphere configuration
        let a = self.alpha;
        let a3 = vec3(a.x, a.y, 1.);
        let flip = out.z < 0.;
        let v = if flip { -out } else { out };
        let vh = a3.component_mul(&v).normalize();

        // Section 4.1: orthonormal basis (with special case if cross product is zero)
        let mut t1_v = vec3(-vh.y, vh.x, 0.);
        t1_v = t1_v.try_normalize(0.0001).unwrap_or_else(Vec3::x);
        let t2_v = vh.cross(&t1_v);
        let tm = na::Matrix3::from_columns(&[t1_v, t2_v, vh]);

        // Section 4.2: parameterization of the projected area
        let r = u.x.sqrt();
        let phi = 2. * PI * u.y;
        let (mut t2, mut t1) = phi.sin_cos();
        t1 *= r;
        t2 *= r;
        let s = 0.5 * (1. + vh.z);
        t2 = (1. - s) * (1. - t1 * t1).sqrt() + s * t2;

        // Section 4.3: reprojection onto hemisphere
        let t3 = (1. - vec2(t1, t2).norm_squared()).max(0.).sqrt();
        let nh = tm * vec3(t1, t2, t3);

        // Section 3.4: transforming the normal back to the ellipsoid configuration
        let mut ne = a3.component_mul(&nh);
        ne.z = ne.z.max(0.);
        ne = ne.normalize();

        if flip {
            -ne
        } else {
            ne
        }
    }
}
pub struct TrowbridgeReitz {
    pub alpha: Vec2,
}

impl TrowbridgeReitz {
    fn sample11(w: Vec3, u: Vec2) -> Vec2 {
        // special case (normal incidence)
        if w.cos_theta() > 0.9999 {
            let r = u.x / (1. - u.x);
            let phi = 2. * PI * u.y;
            return r * phi.cos_sin();
        }

        let tan_theta = w.tan_theta();
        if tan_theta.is_infinite() {
            return w.xy();
        }
        let little_a = tan_theta.recip();
        let g1 = 2. / (1. + (1. + little_a.sqr().recip()).sqrt());

        // sample slope_x
        let a = 2. * u.x / g1 - 1.;
        let tmp = (a.sqr() - 1.).recip().min(1e9);
        let b = tan_theta;
        let d = b.sqr() * tmp.sqr() - (a.sqr() - b.sqr()) * tmp;
        let d = d.max(0.).sqrt();
        let slope_x = Vec2::from_element(b * tmp) + vec2(-d, d);
        let slope_x = if a < 0. || slope_x.y > tan_theta.recip() {
            slope_x.x
        } else {
            slope_x.y
        };

        // sample slope_y
        let (s, v) = if u.x > 0.5 {
            (1., 2. * (u.y - 0.5))
        } else {
            (-1., 2. * (0.5 - u.y))
        };
        let z = (v * (v * (v * 0.27385 - 0.73369) + 0.46341))
            / (v * (v * (v * 0.093073 + 0.309420) - 1.) + 0.597999);
        let slope_y = s * z * (1. + slope_x.sqr()).sqrt();

        vec2(slope_x, slope_y)
    }
}

impl MicrofacetModel for TrowbridgeReitz {
    fn d(&self, half: Vec3) -> f32 {
        let tan2_theta = half.tan_theta().sqr();
        if tan2_theta > 1e9 {
            return 0.;
        }
        let mut cos4_theta = half.cos2_theta();
        cos4_theta *= cos4_theta;

        let alpha_neg2 = self.alpha.map(f32::sqr).map(f32::recip);
        let e = half.cos_sin_phi().map(f32::sqr).dot(&alpha_neg2) * tan2_theta;
        (PI * self.alpha.x * self.alpha.y * cos4_theta * (e + 1.).sqr()).recip()
    }

    fn lambda(&self, w: Vec3) -> f32 {
        let abs_tan_theta = w.tan_theta().abs();
        if abs_tan_theta > 1e9 {
            return 0.;
        }
        // Compute _alpha_ for direction _w_
        let alpha2 = self.alpha.map(f32::sqr);
        let alpha = w.cos_sin_phi().map(f32::sqr).dot(&alpha2);
        let a2t2t = (alpha * abs_tan_theta).sqr();
        return ((1. + a2t2t).sqrt() - 1.) / 2.;
    }

    fn sample(&self, out: Vec3, u: Vec2) -> Vec3 {
        let flip = out.z < 0.;
        let v = if flip { -out } else { out };
        let scale = vec3(self.alpha.x, self.alpha.y, 1.);
        let v = v.component_mul(&scale).normalize();
        let phi = v.cos_sin_phi();
        let slope = na::Matrix2::new(phi.x, -phi.y, phi.y, phi.x) * TrowbridgeReitz::sample11(v, u);
        let normal = vec3(-slope.x, -slope.y, 1.)
            .component_mul(&scale)
            .normalize();
        if flip {
            -normal
        } else {
            normal
        }
    }
}

pub fn shlick_fresnel(cos_theta: f32, f0: f32) -> f32 {
    f0 + (1. - cos_theta).powi(5) * (1. - f0)
}
