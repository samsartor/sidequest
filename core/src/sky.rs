use crate::{
    distr::{CdfSlice, Distr},
    util::{vec2, vec3, IVec2, Vec2, Vec3, HALF_PI},
    SkyVertex, Spectrum,
};
use core::{
    f32::consts::PI,
    ops::{Deref, DerefMut},
};
use nalgebra as na;
use num_traits::Float;
use rand::Rng;
use rustacuda_derive::DeviceCopy;

#[derive(Copy, Clone, Debug, PartialEq, Eq, DeviceCopy)]
#[repr(u8)]
pub enum Filtering {
    Closest = 1,
    Bilinear = 2,
}

#[derive(Copy, Clone, Debug, DeviceCopy)]
#[repr(C)]
pub struct Sky<B> {
    pub radiance: B,
    pub width: u32,
    pub height: u32,
    pub units: f32, // units * mean(energy) * 4pi = watts / m^2
    pub filtering: Filtering,
}

impl<B> Sky<B> {
    #[inline(always)]
    pub fn width(&self) -> usize {
        self.width as usize
    }

    #[inline(always)]
    pub fn height(&self) -> usize {
        self.height as usize
    }

    pub fn map_buffer<'a, A>(&'a self, map: &mut impl FnMut(&'a B) -> A) -> Sky<A>
    where
        B: 'a,
    {
        Sky {
            radiance: map(&self.radiance),
            width: self.width,
            height: self.height,
            units: self.units,
            filtering: self.filtering,
        }
    }

    pub fn map_buffer_mut<'a, A>(&'a mut self, map: &mut impl FnMut(&'a mut B) -> A) -> Sky<A>
    where
        B: 'a,
    {
        Sky {
            radiance: map(&mut self.radiance),
            width: self.width,
            height: self.height,
            units: self.units,
            filtering: self.filtering,
        }
    }
}

impl<B: Deref<Target = [f32]>> Sky<B> {
    // watts / steradian / m^2
    pub fn index(&self, index: IVec2) -> Spectrum {
        let mut i = (index.x % self.width()) + index.y.min(self.height() - 1) * self.width();
        i *= 3;
        na::Vector3::from_column_slice(&self.radiance[i..][..3]) * self.units
    }

    pub fn index_filtered(&self, index: Vec2) -> Spectrum {
        match self.filtering {
            Filtering::Closest => self.index(index.map(|x| Float::round(x) as usize)),
            Filtering::Bilinear => {
                let i = index.map(|x| Float::floor(x) as usize);
                let s = index.map(Float::fract);
                let t = s.map(|x| 1. - x);

                self.index(i) * t.x * t.y
                    + self.index(i + IVec2::x()) * s.x * t.y
                    + self.index(i + IVec2::y()) * t.x * s.y
                    + self.index(i + vec2(1, 1)) * s.x * s.y
            }
        }
    }

    pub fn unit_x(&self) -> f32 {
        2. * PI / self.width as f32
    }

    pub fn unit_y(&self) -> f32 {
        PI / self.height as f32
    }

    pub fn direction_to(&self, index: Vec2) -> Vec3 {
        let azimuth = index.x * self.unit_x() - PI;
        let altitude = index.y * self.unit_y() - HALF_PI;
        let (y, x) = azimuth.sin_cos();
        let (z, r) = altitude.sin_cos();
        vec3(x * r, y * r, -z)
    }

    pub fn looking_at(&self, dir: Vec3) -> Vec2 {
        let mut azimuth = dir.y.atan2(dir.x) + PI;
        azimuth /= self.unit_x();
        let mut altitude = (-dir.z).atan2(dir.xy().magnitude()) + HALF_PI;
        altitude /= self.unit_y();
        vec2(azimuth, altitude)
    }

    pub fn look(&self, dir: Vec3) -> Spectrum {
        self.index_filtered(self.looking_at(dir))
    }

    pub fn irradiance_between(&self, start: IVec2, end: IVec2) -> Spectrum {
        let mut acc = Vec3::zeros();
        let unit_y = self.unit_y();
        for y in start.y..end.y {
            let altitude = (y as f32 + 0.5) * unit_y;
            let unit_x = altitude.sin() * self.unit_x();
            let unit_area = unit_x * unit_y;
            for x in start.x..end.x {
                acc += self.index(vec2(x, y)) * unit_area;
            }
        }
        acc
    }
}

#[derive(Copy, Clone, Debug, DeviceCopy)]
#[repr(C)]
pub struct SkyDistr<B> {
    sky: Sky<B>,
    block_width: u32,
    block_height: u32,
    cdf_row_sums: B,
    blocks_per_row: u32,
    cdf_rows: B,
    irradiance: f32,
}

impl<B> SkyDistr<B> {
    pub fn map_buffer<'a, A>(&'a self, map: &mut impl FnMut(&'a B) -> A) -> SkyDistr<A>
    where
        B: 'a,
    {
        SkyDistr {
            sky: self.sky.map_buffer(map),
            block_width: self.block_width,
            block_height: self.block_height,
            cdf_row_sums: map(&self.cdf_row_sums),
            blocks_per_row: self.blocks_per_row,
            cdf_rows: map(&self.cdf_rows),
            irradiance: self.irradiance,
        }
    }

    pub fn map_buffer_mut<'a, A>(&'a mut self, map: &mut impl FnMut(&'a mut B) -> A) -> SkyDistr<A>
    where
        B: 'a,
    {
        SkyDistr {
            sky: self.sky.map_buffer_mut(map),
            block_width: self.block_width,
            block_height: self.block_height,
            cdf_row_sums: map(&mut self.cdf_row_sums),
            blocks_per_row: self.blocks_per_row,
            cdf_rows: map(&mut self.cdf_rows),
            irradiance: self.irradiance,
        }
    }
}

impl<B: DerefMut<Target = [f32]>> SkyDistr<B> {
    pub fn new(
        sky: Sky<B>,
        mut allocate: impl FnMut(usize) -> B,
        mut blocks_h: usize,
        mut blocks_v: usize,
    ) -> Self {
        let block_width = (sky.width() / blocks_h).max(1);
        let block_height = (sky.height() / blocks_v).max(1);
        blocks_h = sky.width() / block_width;
        blocks_v = sky.height() / block_height;
        let num_blocks = blocks_h * blocks_v;

        let mut cdf_row_sums = allocate(blocks_v);
        let mut cdf_rows = allocate(num_blocks);
        let mut x = 0;
        let mut y = 0;
        let mut row_sum = 0.;
        loop {
            if x >= blocks_h {
                cdf_row_sums[y] = row_sum;
                row_sum = 0.;
                x = 0;
                y += 1;
            }
            if y >= blocks_v {
                break;
            }
            let sky_x = x * block_width;
            let sky_y = y * block_height;
            let radiance = sky
                .irradiance_between(
                    vec2(sky_x, sky_y),
                    vec2(
                        sky.width().min(sky_x + block_width),
                        sky.height().min(sky_y + block_height),
                    ),
                )
                .sum();
            row_sum += radiance;
            cdf_rows[x + y * blocks_h] = radiance;
            x += 1;
        }

        let total = cdf_row_sums.iter().sum::<f32>();

        let divisor = total.recip();
        let mut partial_sum = 0.;
        for x in &mut *cdf_row_sums {
            partial_sum += *x;
            *x = partial_sum * divisor;
        }

        for row in cdf_rows.chunks_mut(blocks_h) {
            let divisor = row.iter().sum::<f32>().recip();
            let mut partial_sum = 0.;
            for x in row {
                partial_sum += *x;
                *x = partial_sum * divisor;
            }
        }

        SkyDistr {
            sky,
            block_width: block_width as u32,
            block_height: block_width as u32,
            cdf_row_sums,
            blocks_per_row: blocks_h as u32,
            cdf_rows,
            irradiance: total,
        }
    }
}

impl<B: Deref<Target = [f32]>> SkyDistr<B> {
    pub fn cdf_row_sums(&self) -> CdfSlice {
        CdfSlice(&self.cdf_row_sums)
    }

    pub fn cdf_rows(&self, row: usize) -> CdfSlice {
        let w = self.blocks_per_row as usize;
        CdfSlice(&self.cdf_rows[row * w..][..w])
    }

    pub fn look(&self, dir: Vec3) -> (f32, Spectrum) {
        let i = self.sky.looking_at(dir);
        let energy = self.sky.index_filtered(i);
        let block_x = i.x as usize / self.block_width as usize;
        let block_y = i.y as usize / self.block_height as usize;
        let p0 = self.cdf_row_sums().p(block_y);
        let p1 = self.cdf_rows(block_y).p(block_x);
        (p0 * p1 / (4. * PI), energy)
    }
}

impl<B> AsRef<Sky<B>> for SkyDistr<B> {
    fn as_ref(&self) -> &Sky<B> {
        &self.sky
    }
}

impl<R: Rng, B: Deref<Target = [f32]>> Distr<SkyVertex, R> for SkyDistr<B> {
    fn sample(&self, rng: &mut R) -> (f32, SkyVertex) {
        let (p0, row) = self.cdf_row_sums().sample(rng);
        let (p1, col) = self.cdf_rows(row).sample(rng);
        let mut block_w = self.block_width as usize;
        let mut block_h = self.block_height as usize;
        let corner = vec2(col * block_w, row * block_h);
        block_w = block_w.min(self.sky.width() - corner.x);
        block_h = block_h.min(self.sky.height() - corner.y);
        let i = corner.map(|x| x as f32)
            + vec2(
                rng.gen_range(0., block_w as f32),
                rng.gen_range(0., block_h as f32),
            );

        (
            p0 * p1 / (4. * PI),
            SkyVertex {
                energy: self.sky.index_filtered(i),
                direction: self.sky.direction_to(i),
            },
        )
    }
}
