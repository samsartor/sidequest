use crate::util::{vec3, Vec3};
use core::f32::consts::PI;
#[allow(unused_imports)]
use num_traits::Float;
use rand::{distributions::Open01, Rng};

pub trait Distr<X, R: Rng> {
    fn sample(&self, rng: &mut R) -> (f32, X);
}
pub struct CosineHemisphere;

impl<R: Rng> Distr<Vec3, R> for CosineHemisphere {
    fn sample(&self, rng: &mut R) -> (f32, Vec3) {
        let u: f32 = rng.gen();
        let v: f32 = rng.gen();
        let r = u.sqrt();
        let theta = 2. * PI * v;
        let (x, y) = theta.sin_cos();
        let z = (1. - u).sqrt();

        (z / PI, vec3(r * x, r * y, z))
    }
}

#[derive(Copy, Clone)]
pub struct CdfSlice<'a>(pub &'a [f32]);

impl<'a> CdfSlice<'a> {
    pub fn get(self, idx: usize) -> f32 {
        self.0.get(idx).copied().unwrap_or(1.)
    }

    pub fn p(self, idx: usize) -> f32 {
        let above = self.get(idx);
        let below = idx.checked_sub(1).map(|i| self.get(i)).unwrap_or(0.);

        (above - below) * self.0.len() as f32
    }
}

impl<'a, R: Rng> Distr<usize, R> for CdfSlice<'a> {
    fn sample(&self, rng: &mut R) -> (f32, usize) {
        use core::cmp::Ordering::*;

        let goal: f32 = rng.sample(Open01);
        let idx = self
            .0
            .binary_search_by(|&x| if x < goal { Less } else { Greater })
            .unwrap_err();

        (self.p(idx), idx)
    }
}
