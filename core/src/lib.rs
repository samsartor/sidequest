#![no_std]
#![cfg_attr(
    any(target_arch = "nvptx", target_arch = "nvptx64"),
    feature(abi_ptx, stdsimd)
)]

use core::f32::consts::PI;
use core::{cell::UnsafeCell, ops::Deref};
use distr::{CosineHemisphere, Distr};
use material::{shlick_fresnel, MicrofacetModel, Ggx};
use na::{Isometry3, Matrix3, Point3};
use nalgebra as na;
#[allow(unused_imports)]
use num_traits::Float;
use path::{Path, Strategy, Strategy::*, StrategySamples};
use rand::Rng;
use rustacuda_derive::DeviceCopy;
use sky::SkyDistr;
use util::{vec2, vec3, Cpy, DeviceBufferRef, Vec3};

pub mod aces;
pub mod distr;
pub mod estimate;
#[cfg(any(target_arch = "nvptx", target_arch = "nvptx64"))]
mod kernel;
pub mod material;
pub mod path;
pub mod sky;
pub mod util;

pub type Estimator = estimate::LeastSquaresEstimator;

#[derive(DeviceCopy, Copy, Clone, Debug, PartialEq, Eq)]
#[repr(C)]
pub enum DiffVariable {
    Roughness,
    Radius,
}

#[derive(DeviceCopy)]
#[repr(C)]
pub struct KernelParams<'a> {
    pub seed: u32,
    pub view: ViewParams,
    pub engine: Engine<DeviceBufferRef<'a, f32>>,
    pub buffer: DeviceBufferRef<'a, UnsafeCell<Estimator>>,
}

#[derive(Clone, Debug, Copy)]
pub struct Ray {
    pub origin: Point3<f32>,
    pub dir: Vec3,
}

pub type Spectrum = Vec3;

impl Ray {
    pub fn at(self, t: f32) -> Point3<f32> {
        self.origin + self.dir * t
    }
}

#[derive(Copy, Clone, Debug)]
pub struct SkyVertex {
    pub direction: Vec3,
    pub energy: Spectrum,
}

#[derive(Copy, Clone, Debug)]
pub struct SurfaceVertex {
    pub point: Point3<f32>,
    pub normal: Vec3,
    pub tangent: Vec3,
    pub albedo: Spectrum,
    pub fresnel0: f32,
    pub roughness: f32,
    pub depth: f32,
}

pub type CameraVertex = Ray;

#[derive(Clone, Copy, Debug, DeviceCopy)]
#[repr(C)]
pub struct Perspective {
    pub left: f32,
    pub right: f32,
    pub top: f32,
    pub bottom: f32,
    pub near: f32,
    pub far: f32,
}

#[derive(Clone, Debug, DeviceCopy)]
#[repr(C)]
pub struct ViewParams {
    pub perspective: Perspective,
    pub isometry: Cpy<Isometry3<f32>>,
    pub diff: DiffVariable,
    pub width: u32,
    pub height: u32,
}

impl ViewParams {
    #[rustfmt::skip]
    pub fn camera_ray(&self, x: usize, y: usize) -> CameraRay {
        let p = self.perspective;
        let i = self.isometry.to();
        let cx = x as f32 / self.width as f32;
        let cy = y as f32 / self.height as f32;

        let x = p.left * (1. - cx) + p.right * cx;
        let y = p.top * (1. - cy) + p.bottom * cy;
        let z = p.near;
        let w = p.right - p.left;
        let h = p.bottom - p.top;

        CameraRay {
            origin: i * Point3::origin(),
            matrix: Matrix3::from_columns(&[
                i.rotation * vec3(w / self.width as f32, 0., 0.),
                i.rotation * vec3(0., h / self.height as f32, 0.),
                i.rotation * vec3(x, y, z),
            ]),
        }
    }
}

#[derive(Clone, Debug, Copy)]
pub struct CameraRay {
    origin: Point3<f32>,
    matrix: Matrix3<f32>,
}

impl CameraRay {
    pub fn center(&self) -> CameraVertex {
        CameraVertex {
            origin: self.origin,
            dir: (self.matrix * vec3(0.5, 0.5, 1.)).normalize(),
        }
    }
}

impl<R: Rng> Distr<CameraVertex, R> for CameraRay {
    fn sample(&self, rng: &mut R) -> (f32, CameraVertex) {
        (
            1.,
            CameraVertex {
                origin: self.origin,
                dir: (self.matrix * vec3(rng.gen(), rng.gen(), 1.)).normalize(),
            },
        )
    }
}

type Hit = Option<SurfaceVertex>;

fn hit_sphere(center: Point3<f32>, radius: f32, ray: Ray) -> Hit {
    // From: https://raytracing.github.io/books/RayTracingInOneWeekend.html#addingasphere
    let oc = ray.origin - center;
    let a = ray.dir.magnitude_squared();
    let b = 2. * oc.dot(&ray.dir);
    let c = oc.magnitude_squared() - radius * radius;
    let disc = b * b - 4. * a * c;
    if disc < 0. {
        return None;
    }
    let t = (-b - disc.sqrt()) / (2. * a);
    if t < 0.01 {
        return None;
    }
    let p = ray.at(t);
    let n = (p - center).normalize();
    let tan = vec3(n.y, -n.x, 0.)
        .try_normalize(1e-4)
        .unwrap_or_else(Vec3::x);
    Some(SurfaceVertex {
        depth: t,
        point: p,
        tangent: tan,
        normal: n,
        albedo: vec3(0.7, 0.7, 0.7),
        fresnel0: 0.75,
        roughness: 0.1,
    })
}

#[derive(Copy, Clone, Debug, DeviceCopy)]
#[repr(C)]
// TODO: rename to Scene
pub struct Engine<B> {
    pub sky: SkyDistr<B>,
    pub samples: StrategySamples,
    pub include: bool,
}

impl<B> Engine<B> {
    pub fn map_buffer<'a, A>(&'a self, map: &mut impl FnMut(&'a B) -> A) -> Engine<A>
    where
        B: 'a,
    {
        Engine {
            sky: self.sky.map_buffer(map),
            samples: self.samples,
            include: self.include,
        }
    }

    pub fn map_buffer_mut<'a, A>(&'a mut self, map: &mut impl FnMut(&'a mut B) -> A) -> Engine<A>
    where
        A: 'a,
    {
        Engine {
            sky: self.sky.map_buffer_mut(map),
            samples: self.samples,
            include: self.include,
        }
    }
}

impl<B: Deref<Target = [f32]>> Engine<B> {
    fn hit_scene(&self, ray: Ray, radius_off: f32) -> Hit {
        if !self.include {
            return None;
        }

        const R: f32 = 3.0;

        let mut hit: Hit = None;
        for &center in &[
            Point3::new(R, R, 0.),
            Point3::new(-R, R, 0.),
            Point3::new(R, -R, 0.),
            Point3::new(-R, -R, 0.),
        ] {
            if let Some(this_hit) = hit_sphere(center, 2. + radius_off, ray) {
                if hit.map(|h| h.depth > this_hit.depth).unwrap_or(true) {
                    hit = Some(this_hit);
                }
            }
        }

        hit
    }

    fn create_path(
        &self,
        ray: Ray,
        actual: Strategy,
        v: SurfaceVertex,
        alpha_mul: f32,
        rng: &mut impl Rng,
    ) -> Path {
        let into_world =
            na::Matrix3::from_columns(&[v.normal.cross(&v.tangent), v.tangent, v.normal]);
        let from_world = into_world.transpose();
        let output = from_world * -ray.dir;
        let mfm = Ggx {
            alpha: vec2(v.roughness, v.roughness) * alpha_mul,
        };

        let mut look = None;

        let input = match actual {
            LightToSurface => {
                let (p, i) = self.sky.sample(rng);
                look = Some((p, i.energy));
                from_world * i.direction
            }
            DiffuseToLight => {
                let (_, l) = Distr::sample(&CosineHemisphere, rng);
                l
            }
            GlossToLight => {
                let n = mfm.sample(output, rng.gen());
                2. * output.dot(&n) * n - output
            }
        };

        let (p_light_to_surface, incoming_energy) =
            look.unwrap_or_else(|| self.sky.look(into_world * input));

        let mut energy = Vec3::zeros();
        let cos_theta = input.z.max(0.);

        // Lambertian diffuse
        energy += incoming_energy.component_mul(&v.albedo) / PI;

        // Glossy
        let mut p_gloss_to_light = 1.;
        let half = input + output;
        if let Some(half) = half.try_normalize(1e-9) {
            // Cook-Torrance
            let four_o_dot_h = 4. * output.dot(&half);
            energy += incoming_energy
                * mfm.d(half)
                * shlick_fresnel(output.dot(&half), v.fresnel0)
                * mfm.g(output, input)
                / (four_o_dot_h * input.dot(&half));
            p_gloss_to_light = mfm.pdf(output, half) / four_o_dot_h;
        }

        Path {
            energy: energy * cos_theta,
            p_light_to_surface,
            p_diffuse_to_light: cos_theta / PI,
            p_gloss_to_light,
            actual,
        }
    }

    pub fn estimate_ray(
        &self,
        ray: CameraRay,
        rng: &mut impl Rng,
        diffvar: DiffVariable,
        estimator: &mut impl estimate::Estimator<f32>,
    ) {
        let num = self.samples.total();
        for strategy in self.samples.iter() {
            let ray = match num {
                1 => ray.center(),
                _ => ray.sample(rng).1,
            };

            let y = rng.gen_range(-1., 1.);
            let (radius, alpha) = match diffvar {
                DiffVariable::Radius => (y * 0.05, 1.),
                DiffVariable::Roughness => (0., y.exp2()),
            };

            if let Some(v) = self.hit_scene(ray, radius) {
                let path = self.create_path(ray, strategy, v, alpha, rng);
                estimator.accumulate(y, 0.5, path.energy, path.multiplier(self.samples));
            } else {
                estimator.accumulate(y, 0.5, self.sky.as_ref().look(ray.dir), 1.);
            }
        }
    }
}
