use core::f32::consts::PI;
use core::hash::{Hash, Hasher};
use core::{
    cell::UnsafeCell,
    convert::TryInto,
    marker::PhantomData,
    ops::{Add, AddAssign, Index, IndexMut},
    slice::from_raw_parts,
};
use nalgebra as na;
use num_traits::Float;
use rand::Rng;
use rustacuda_core::{DeviceCopy, DevicePointer};
use rustacuda_derive::DeviceCopy;

pub const HALF_PI: f32 = PI / 2.;
pub type IVec2 = na::Vector2<usize>;
pub type Vec2 = na::Vector2<f32>;
pub type Vec3 = na::Vector3<f32>;

#[inline(always)]
pub fn vec2<N: na::Scalar>(x: N, y: N) -> na::Vector2<N> {
    na::Vector2::new(x, y)
}

#[inline(always)]
pub fn vec3<N: na::Scalar>(x: N, y: N, z: N) -> na::Vector3<N> {
    na::Vector3::new(x, y, z)
}

pub trait HasDeviceCopy {
    type Repr: Copy + DeviceCopy;

    fn from_copy(repr: Self::Repr) -> Self;
    fn to_copy(self) -> Self::Repr;
}

impl HasDeviceCopy for Vec2 {
    type Repr = [f32; 2];

    #[inline(always)]
    fn from_copy(repr: [f32; 2]) -> Self {
        repr.into()
    }

    #[inline(always)]
    fn to_copy(self) -> [f32; 2] {
        self.into()
    }
}

impl HasDeviceCopy for Vec3 {
    type Repr = [f32; 3];

    #[inline(always)]
    fn from_copy(repr: [f32; 3]) -> Self {
        repr.into()
    }

    #[inline(always)]
    fn to_copy(self) -> [f32; 3] {
        self.into()
    }
}

impl HasDeviceCopy for na::Point3<f32> {
    type Repr = [f32; 3];

    #[inline(always)]
    fn from_copy(repr: [f32; 3]) -> Self {
        repr.into()
    }

    #[inline(always)]
    fn to_copy(self) -> [f32; 3] {
        self.coords.into()
    }
}

impl HasDeviceCopy for na::Isometry3<f32> {
    type Repr = [f32; 7];

    #[inline(always)]
    fn from_copy(repr: [f32; 7]) -> Self {
        let t = Vec3::from_column_slice(&repr[..3]);
        let r = na::Vector4::from_column_slice(&repr[3..]);
        na::Isometry3 {
            translation: t.into(),
            rotation: na::Unit::new_unchecked(r.into()),
        }
    }

    #[inline(always)]
    fn to_copy(self) -> [f32; 7] {
        let mut repr = [0.; 7];
        repr[..3].copy_from_slice(self.translation.vector.as_ref());
        repr[3..].copy_from_slice(self.rotation.as_vector().as_ref());
        repr
    }
}

impl HasDeviceCopy for na::Matrix3<f32> {
    type Repr = [f32; 9];

    #[inline(always)]
    fn from_copy(repr: [f32; 9]) -> Self {
        Self::from_column_slice(&repr)
    }

    #[inline(always)]
    fn to_copy(self) -> [f32; 9] {
        self.data.as_slice().try_into().unwrap()
    }
}

#[derive(Copy, Clone, Debug, Default)]
#[repr(transparent)]
pub struct Cpy<T: HasDeviceCopy> {
    pub inner: T::Repr,
}

// Cpy is transparent T::Repr -> guaranteed by HasDeviceCopy to be DeviceCopy
unsafe impl<T: HasDeviceCopy> DeviceCopy for Cpy<T> {}

impl<T: HasDeviceCopy> Cpy<T> {
    #[inline(always)]
    pub fn to(&self) -> T {
        T::from_copy(self.inner)
    }

    #[inline(always)]
    pub fn assign(&mut self, inner: T) {
        *self = inner.into();
    }
}

impl<T: HasDeviceCopy> From<T> for Cpy<T> {
    #[inline(always)]
    fn from(val: T) -> Cpy<T> {
        Cpy {
            inner: T::to_copy(val),
        }
    }
}

impl<T: HasDeviceCopy, I> Index<I> for Cpy<T>
where
    T::Repr: Index<I>,
{
    type Output = <T::Repr as Index<I>>::Output;

    fn index(&self, index: I) -> &Self::Output {
        &self.inner[index]
    }
}

impl<T: HasDeviceCopy, I> IndexMut<I> for Cpy<T>
where
    T::Repr: IndexMut<I>,
{
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        &mut self.inner[index]
    }
}

impl<T: HasDeviceCopy> AddAssign<T> for Cpy<T>
where
    T: Add<T, Output = T>,
{
    fn add_assign(&mut self, rhs: T) {
        self.assign(self.to() + rhs);
    }
}

#[derive(DeviceCopy)]
#[repr(C)]
pub struct DeviceBufferRef<'a, T> {
    data: DevicePointer<T>,
    len: u32,
    _ref: PhantomData<&'a [T]>,
}

impl<'a, T> DeviceBufferRef<'a, T> {
    pub fn from_raw(data: DevicePointer<T>, len: u32) -> Self {
        DeviceBufferRef {
            data,
            len,
            _ref: PhantomData,
        }
    }

    /// This is unsound if:
    /// - The data pointer is invalid
    /// - The backing storage is uninitialized
    /// - The returned slice is concurrent with a ref returned by get_mut
    pub unsafe fn as_slice(&self) -> &'a [T] {
        from_raw_parts(self.data.as_raw(), self.len as usize)
    }
}

impl<'a, T> DeviceBufferRef<'a, UnsafeCell<T>> {
    unsafe fn to_ptr(&self) -> *mut T {
        (*self.data.as_raw()).get()
    }

    /// This is unsound if:
    /// - The data pointer is invalid
    /// - The backing storage is uninitialized
    /// - The returned ref is concurrent with a slice from as_slice or the same
    ///   ref from a different call to get_mut
    pub unsafe fn get_mut(&self, at: usize) -> &mut T {
        assert!(at < self.len as usize, "index out of bounds");
        &mut *self.to_ptr().offset(at as isize)
    }
}

pub trait Vec3Ext {
    fn cos_theta(&self) -> f32;
    fn cos2_theta(&self) -> f32;
    fn abs_cos_theta(&self) -> f32;
    fn sin2_theta(&self) -> f32;
    fn sin_theta(&self) -> f32;
    fn tan_theta(&self) -> f32;
    fn cos_sin_phi(&self) -> Vec2;
}

impl Vec3Ext for Vec3 {
    fn cos_theta(&self) -> f32 {
        self.z
    }
    fn cos2_theta(&self) -> f32 {
        self.z.sqr()
    }
    fn abs_cos_theta(&self) -> f32 {
        self.z.abs()
    }
    fn sin2_theta(&self) -> f32 {
        (1. - self.cos2_theta()).max(0.)
    }
    fn sin_theta(&self) -> f32 {
        self.sin2_theta().sqrt()
    }
    fn tan_theta(&self) -> f32 {
        self.sin_theta() / self.cos_theta()
    }

    fn cos_sin_phi(&self) -> Vec2 {
        let s = self.sin_theta();
        if s < 1e-9 {
            Vec2::x()
        } else {
            (self.xy() / s).map(|x| x.min(1.).max(-1.))
        }
    }
}

pub trait FloatExt: na::Scalar {
    fn sqr(self) -> Self;
    fn cos_sin(self) -> na::Vector2<Self>;
}

impl<F: Float + na::Scalar> FloatExt for F {
    fn sqr(self) -> Self {
        self * self
    }
    fn cos_sin(self) -> na::Vector2<Self> {
        let (y, x) = self.sin_cos();
        vec2(x, y)
    }
}

pub fn rng_at<H: Hash>(seed: H, x: H, y: H) -> impl Rng {
    use rand::SeedableRng;

    let mut hash = fnv::FnvHasher::default();
    seed.hash(&mut hash);
    x.hash(&mut hash);
    y.hash(&mut hash);
    rand::rngs::SmallRng::seed_from_u64(hash.finish())
}
